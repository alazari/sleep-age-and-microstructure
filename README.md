# Individual differences in slow wave sleep architecture relate to variation in white matter microstructure across adulthood

Repository for code from 'Individual differences in slow wave sleep architecture relate to variation in white matter microstructure across adulthood' by Gudberg et al.
