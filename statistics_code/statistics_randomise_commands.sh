#!/usr/bin/env bash

analysisDir=vols/Scratch
mask=$analysisDir/mean_FA_skeletonised_mask.nii.gz
stat_mat=$analysisDir/independent_samples_ttest.mat
stat_con=$analysisDir/independent_samples_ttest.con

mkdir -p $outDir
mkdir -p $outDir/MT

randomise -i $analysisDir/FA_skeletonised.nii.gz \
-o $analysisDir/age_SW_FA \
-m $mask \
-d $stat_mat \
-t $stat_con  \
--T2 -D -n 5000

randomise -i $analysisDir/MD_skeletonised.nii.gz \
-o $analysisDir/age_SW_MD \
-m $mask \
-d $stat_mat \
-t $stat_con  \
--T2 -D -n 5000

randomise -i $analysisDir/RD_skeletonised.nii.gz \
-o $analysisDir/age_SW_RD \
-m $mask \
-d $stat_mat \
-t $stat_con  \
--T2 -D -n 5000

randomise -i $analysisDir/AD_skeletonised.nii.gz \
-o $analysisDir/age_SW_AD \
-m $mask \
-d $stat_mat \
-t $stat_con  \
--T2 -D -n 5000
